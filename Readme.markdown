QR Code generator library - SCHEME R7RS-small
=============================================

This project aims to be a simple R7RS-small implementation of QR code generator.

This work is an adaptation of the work of Nayuki:
[Project Nayuki](https://www.nayuki.io/page/qr-code-generator-library)
and is also distributed under MIT License.

It is tested using the interpreter TR7 (https://gitlab.com/jobol/tr7)

Example of use: `tr7i qrcodegen-demo.scm`

Guile users should also check
[Artyom Poptsov's fork for GNU Guile](https://github.com/artyom-poptsov/guile-qr-code)

Improvements for other implementations are very welcome

Author/Adaptation José Bollo aka jobol

